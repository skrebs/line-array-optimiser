import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import cmath

# Fixed Parameters given by real world problem:
speaker_size = 0.213 #in meters
np.set_printoptions(precision=3)


def intensity_to_dB(intensity):
    return 20 * np.log10(intensity)


def dB_to_intensity(spl):
    return 10 ** (spl/20)


def generate_speaker_positions(trim, splay_angles):
    posx = 0
    posy = trim
    theta = 0
    speaker_positions = []

    for speaker in range(len(splay_angles)):
        theta += splay_angles[speaker]
        posx -= np.sin(np.deg2rad(theta)) * speaker_size/2
        posy -= np.cos(np.deg2rad(theta)) * speaker_size/2
        speaker_positions.append([posx, posy, np.deg2rad(theta)])
        posx -= np.sin(np.deg2rad(theta)) * speaker_size/2
        posy -= np.cos(np.deg2rad(theta)) * speaker_size/2

    return speaker_positions


def speaker_directivity_attenuation(frequency, angle):
    #off axis (180°) attenuation at freq
    atten = [0   , 0 , 0  , -6 , -12, -24 , -36 , -36 , -42 , -42  ]
    freq  = [31.5, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000]

    SPL_min = np.interp(frequency, freq, atten)

    x = [20, 125, 500, 2000, 20000]
    y = [20, 100, 2300,  10000, 15000]

    return np.maximum(dB_to_intensity(SPL_min), 0.5 * (1 + np.cos(np.minimum(np.pi,(np.interp(frequency, x,y)-100)*7/15900 * abs(angle)))))


def speaker_frequency_response(frequency):
    freqs = [20, 25.3, 33.7, 43.8, 85, 113.8, 365, 455, 568, 802, 1030, 1390, 1860, 4200, 5370, 6250, 9450, 11950, 13700, 20000]
    spl = [79.9, 81.7, 72.4, 73, 106.5, 106, 108, 106.7, 108.7, 108.5, 107.6, 109.2, 110, 110, 112, 111.6, 115.8, 114.7, 116.5, 100] #in dB

    return dB_to_intensity(np.interp(frequency, freqs, spl))


def geometric_spreading_attenuation(distance):
    return 1/distance


def atmospheric_absorbtion_attenuation(frequency, distance):
    # Values at 15°C and 25% rel humidity
    freqs = [20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 20480]
    atten = [0.000029, 0.000108, 0.000347, 0.000806, 0.001488, 0.003251, 0.009807, 0.034233, 0.111094, 0.266156, 0.44215] #in dB/m

    return dB_to_intensity(-np.interp(frequency, freqs, atten) * distance)


def calculate_relative_positions(speaker_positions, mic_position):
    deltaX = mic_position[0] - speaker_positions[:,0]
    deltaY = mic_position[1] - speaker_positions[:,1]

    d = np.sqrt(deltaX*deltaX + deltaY*deltaY)
    phi = np.arctan2(-deltaY, deltaX) - speaker_positions[:,2]

    return d, phi


def calculate_phase(frequencies, distance, delays):
    t_tot = distance / 340.2 + np.array(delays)/1000 #340.2 = speed of sound in 15°C air
    return np.outer(frequencies, t_tot) * 2*np.pi


def phase_sensitive_addition(intensities, phases):
    z_tot = 0+0j

    for i in range(len(intensities)):
        z_tot += cmath.rect(intensities[i], phases[i])

    return np.abs(z_tot)


def calculate_frequency_response(speaker_positions, gains, delays, mic, frequencies):
    d, phi = calculate_relative_positions(speaker_positions, mic)
    phases = calculate_phase(frequencies, d, delays) #-d[0]/340.2)

    all_intensities = []
    for i in range(len(speaker_positions)):
        intensities = dB_to_intensity(gains[i])
        intensities *= speaker_frequency_response(frequencies)
        intensities *= speaker_directivity_attenuation(frequencies, phi[i])
        intensities *= geometric_spreading_attenuation(d[i])
        intensities *= atmospheric_absorbtion_attenuation(frequencies, d[i])
        all_intensities.append(intensities)

    all_intensities = np.array(all_intensities)

    tot_intensities = []

    for i, frequency in enumerate(frequencies):
        tot_intensities.append(phase_sensitive_addition(all_intensities[:,i], phases[i,:]))

    return intensity_to_dB(tot_intensities)


def plot_audience_spl(trim_height, splay_angles, gains, delays):
    frequencies = np.array([250, 500, 1000, 2000, 4000, 8000, 16000])

    speaker_positions = np.array(generate_speaker_positions(trim_height, splay_angles))

    mic_positions = [np.arange(0,20,0.1),np.array([1.7]*len(np.arange(0,20,0.1)))]
    mic_positions = np.swapaxes(mic_positions,0,1)

    frequency_responses = []
    for mic_position in mic_positions:
        frequency_responses.append(calculate_frequency_response(speaker_positions, gains, delays, mic_position, frequencies))

 

    frequency_responses = np.array(frequency_responses)

    for i, frequency in enumerate(frequencies):
        plt.plot(mic_positions[:,0], frequency_responses[:,i], label=str(frequency)+" Hz")

    plt.plot(mic_positions[:,0], np.average(frequency_responses, axis = 1), label = "average over all freq")

    ax = plt.gca()
    plt.legend(loc="lower right")
    ax.set(ylim=(70, 110))
    plt.title("SPL at 1.7m height vs dist")
    plt.xlabel("dist (m)")
    plt.ylabel("SPL (dB)")
    plt.savefig("img/01_audience_spl.png")
    plt.show()
    
    return


def spl_plot(speaker_positions, gains, delays, mic):
    frequencies = np.logspace(np.log10(20),np.log10(20480), num = 121) #20Hz to 20kHz
    frequency_response = calculate_frequency_response(speaker_positions, gains, delays, mic, frequencies)
    plt.plot(frequencies, frequency_response)

    frequencies = np.logspace(np.log10(100),np.log10(16000), num = 121) #20Hz to 20kHz
    frequency_response = calculate_frequency_response(speaker_positions, gains, delays, mic, frequencies)
    plt.axhline(y=np.average(frequency_response), color='r', linestyle='-')
    
    ax = plt.gca()
    ax.set(ylim=(70, 110))
    ax.set_xscale('log')
    ax.set_xticks([31, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000])
    ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    plt.title("Frequency Response at 10m dist")
    plt.xlabel("freq (Hz)")
    plt.ylabel("SPL (Db)")
    plt.savefig("img/02_frequency_response.png")
    plt.show()


def visualize(trim_height, splay_angles, gains, delays, frequency):
    from pylab import linspace, meshgrid, sqrt, log10, angle, cos

    speaker_positions = np.array(generate_speaker_positions(trim_height, splay_angles))

    x = linspace(0, 7.0, 80)
    y = linspace(-1, 20, 210)
    X, Y = meshgrid(x, y)

    def SPL_Matrix(speakers, gains, delays, frequency, x, y):
        spl_matrix = []
        for y2 in y:
            # Create a new row
            spl_matrix.append([])
            for x2 in x:
                Db = calculate_frequency_response(speakers, gains, delays, [y2, x2], frequency)
                spl_matrix[-1].append(np.minimum(120,Db[0]))
        return spl_matrix


    Z = SPL_Matrix(speaker_positions, gains, delays, [frequency], x, y)

    plt.figure()
    ax = plt.gca()
    ax.axis('equal')
    ax.set(xlim=(-1, 20), ylim=(0, 7))
    #Need to draw the contour twice, once for the lines (in 10 sections)
    CS = plt.contour(Y, X, Z, [73, 79, 85, 91, 97, 103, 109, 115, 121], linewidths=0.5, colors='k')
    #And again for the fills
    CS = plt.contourf(Y, X, Z, [73, 79, 85, 91, 97, 103, 109, 115, 121])
    CB = plt.colorbar(CS, orientation='horizontal')
    plt.title("SPL at "+ str(frequency)+" Hz (Db)")
    plt.xlabel("dist (m)")
    plt.ylabel("height (m)")
    plt.savefig("img/03_sideplot.png")
    plt.show()


def g(x):
    if x < -1:
        return -2*x-1
    else:
        return x*x
    

def costfunction(trim_height, splay_angles, gains, delays, mics, frequencies):
    #generate the actual speaker positions from the trim and splay
    speaker_positions = np.array(generate_speaker_positions(trim_height, splay_angles))

    # First generate all SPL Measurements
    frequency_responses = []
    for mic in mics:
        frequency_responses.append(calculate_frequency_response(speaker_positions, gains, delays, mic, frequencies))

    # Then Calculate cost function
    average_SPL = np.average(frequency_responses)

    #print("average SPL: ", average_SPL)

    alpha = 0.4 # weighting of SPL Consistency

    total_cost = 0
    SPL_cost = 0
    freq_cost = 0
    for frequency_response in frequency_responses:
        local_average_SPL = np.average(frequency_response)
    
        for frequency in frequency_response:
            freq_cost += g(frequency - local_average_SPL)
        SPL_cost += ((local_average_SPL - average_SPL) ** 2)
        
    total_cost += freq_cost/len(frequency_response)
    total_cost += alpha * SPL_cost
    total_cost /= len(frequency_responses)

    total_cost += sum(delays)
    return total_cost


def parameters_valid(parameters):
    if parameters[0] > 6 or parameters[0] < 1:
        return False
    if parameters[1] < -10 or parameters[1] > 90:
        return False
    if (parameters[2:6] < 0.5).any() or (parameters[2:6]>11).any():
        return False
    if (parameters[6:11] < -6).any() or (parameters[6:11]>6).any():
        return False
    if (parameters[11:16] < 0).any() or (parameters[11:16]>100).any():
        return False
    
    return True


def evaluate_parameters(parameters, mics, frequencies): # helper function to unpack the parameter
    if not parameters_valid(parameters):
        return 100000
    return costfunction(parameters[0], parameters[1:6], parameters[6:11], parameters[11:16], mics, frequencies)


def plot_vs_ref(parameters_ref, parameters_opt):
    frequencies = np.logspace(np.log10(100),np.log10(16000), num = 121) #20Hz to 20kHz

    mic_positions = [np.arange(0,20,0.1),np.array([1.7]*len(np.arange(0,20,0.1)))]
    mic_positions = np.swapaxes(mic_positions,0,1)

    frequency_responses_ref = []
    frequency_responses_opt = []
    for mic_position in mic_positions:
        frequency_responses_ref.append(calculate_frequency_response(np.array(generate_speaker_positions(parameters_ref[0], parameters_ref[1:6])), parameters_ref[6:11], parameters_ref[11:16], mic_position, frequencies))
        frequency_responses_opt.append(calculate_frequency_response(np.array(generate_speaker_positions(parameters_opt[0], parameters_opt[1:6])), parameters_opt[6:11], parameters_opt[11:16], mic_position, frequencies))
 

    frequency_responses_ref = np.array(frequency_responses_ref)
    frequency_responses_opt = np.array(frequency_responses_opt)

    # for i, frequency in enumerate(frequencies):
    #     plt.plot(mic_positions[:,0], frequency_responses[:,i], label=str(frequency)+" Hz")

    plt.plot(mic_positions[:,0], np.average(frequency_responses_ref, axis = 1)-np.average(frequency_responses_ref), label = "Initial Guess")
    plt.plot(mic_positions[:,0], np.average(frequency_responses_opt, axis = 1)-np.average(frequency_responses_opt), label = "Optimised")

    ax = plt.gca()
    plt.legend(loc="lower right")
    #ax.set_xticks([31, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000])
    #ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    plt.title("relative SPL at 1.7m height vs dist")
    plt.xlabel("dist (m)")
    plt.ylabel("SPL (dB)")
    plt.savefig("img/01_audience_spl.png")
    plt.show()
    
    return


def obj(p):
    mic_positions = [np.arange(2,20,0.1),np.array([1.7]*len(np.arange(2,20,0.1)))]
    mic_positions = np.swapaxes(mic_positions,0,1)
    frequencies = np.logspace(np.log10(100),np.log10(16000), num = 121) #20Hz to 20kHz
    return evaluate_parameters(p, mic_positions, frequencies)


def DOWNHILL_SIMPLEX(parameters):
    from random import uniform, choice

    ### PARAMETERS THAT DEFINE THE OPTIMISATION PROBLEM
    mic_positions = [np.arange(2,20,0.1),np.array([1.7]*len(np.arange(2,20,0.1)))]
    mic_positions = np.swapaxes(mic_positions,0,1)
    frequencies = np.logspace(np.log10(100),np.log10(16000), num = 121) #20Hz to 20kHz

    num_speakers = 5
    num_parameters = num_speakers * 3 

    ### Optimisation parameters

    # wikipedia:
    # \alpha =1, 
    # \gamma = 2, 
    # \rho =1/2} 
    # \sigma =1/2}.

    alpha = 1.1
    beta  = 2
    gamma = 0.6
    sigma = 0.51


    p_perfect = None
    for i in range(20):
        print("random restart ", i+1)

    ### FROM HERE ON YOU DON'T HAVE TO CHANGE STUFF
        simplex = []

        # generate n+1 random initialisations
        for i in range(num_parameters + 1):
            simplex_corner = np.random.normal(parameters, 0.2)
            #simplex_corner[11:16] = np.random.exponential(1,size=(1,5))
            # Evaluate initialisations
            simplex.append([simplex_corner, evaluate_parameters(simplex_corner, mic_positions, frequencies)])


        def find_barycenter(simplex):
            p_bary = np.zeros_like(simplex[0][0])
            for p in simplex:
                p_bary += p[0]
            p_bary /= len(simplex)
            return p_bary

        p_best_scores = [100000]*100
        while True:
            simplex.sort(key = lambda x: x[1])

            p_best = simplex[0]
            print("\rcurrent_min: ", p_best[1], end='')
            p_worst = simplex[-1]
            p_secondworst = simplex[-2]

            p_bary = [find_barycenter(simplex[:-1]),0]

            p_cand1 = p_bary[0] + alpha*(p_bary[0]-p_worst[0])
            p_cand1 = [p_cand1, evaluate_parameters(p_cand1, mic_positions, frequencies)]

            if p_cand1[1] < p_best[1]:
                p_cand2 = p_bary[0] + beta*(p_bary[0]-p_worst[0])
                p_cand2 = [p_cand2, evaluate_parameters(p_cand2, mic_positions, frequencies)]

                if p_cand2[1] < p_cand1[1]:
                    #print("expanding")
                    simplex[-1] = p_cand2
                else:
                    #print("reflecting")
                    simplex[-1] = p_cand1

            elif p_cand1[1] < p_secondworst[1]:
                #print("p_cand is good but not best")
                simplex[-1] = p_cand1
            
            elif p_cand1[1] >= p_secondworst[1]:
                p_better = min(p_worst, p_cand1, key = lambda x: x[1])
                p_cand2 = p_better[0] + gamma*(p_bary[0]-p_better[0])
                p_cand2 = [p_cand2, evaluate_parameters(p_cand2, mic_positions, frequencies)]

                if p_cand2[1] < p_worst[1]:
                    #print("contracting")
                    simplex[-1] = p_cand2
                
                else:
                    #print("shrinking")
                    for i in range(1, len(simplex)):
                        p = simplex[i]
                        p = p[0] + sigma*(p_best[0]-p[0])
                        p = [p, evaluate_parameters(p, mic_positions, frequencies)]
                        simplex[i] = p

            p_best_scores.append(p_best[1]) 
            if  p_best_scores[0] - p_best_scores[100] < 0.001:
                if p_perfect == None or p_best[1] < p_perfect[1]:
                    p_perfect = p_best
                print(p_best)
                parameters = p_perfect[0]
                break
            p_best_scores.pop(0)

    return p_perfect[0]
    
import time

trim_height = 5.5
splay_angles = np.array([10,2,4,6,8])
gains = np.array([0,0,0,0,0])
delays = np.array([1,1,1,1,1])
mic_positions = [np.arange(2,20,0.1),np.array([1.7]*len(np.arange(2,20,0.1)))]
mic_positions = np.swapaxes(mic_positions,0,1)
frequencies = np.logspace(np.log10(100),np.log10(16000), num = 121) #20Hz to 20kHz
start = time.time()
print("reference cost: ", costfunction(trim_height, splay_angles, gains, delays, mic_positions, frequencies))
end = time.time()
print("execution time: ", end - start)

plot_audience_spl(trim_height, splay_angles, gains, delays)
spl_plot(np.array(generate_speaker_positions(trim_height, splay_angles)), gains, delays, [10,1.7])
visualize(trim_height, splay_angles, gains, delays, 2000)

p_best = DOWNHILL_SIMPLEX(np.array([5.5, 10,2,4,6,8, 0,0,0,0,0, 1,1,1,1,1]))

print(p_best)
print(obj(p_best))
plot_vs_ref(np.array([4.99, 9.14,2.7,1.87,7.85, 5.73, -1.85,-1.21,2.26,0.69,0.93, 0.17,0.16,0.12,0.06,1]), p_best)
plot_audience_spl(p_best[0], p_best[1:6], p_best[6:11], p_best[11:16])
spl_plot(np.array(generate_speaker_positions(p_best[0],p_best[1:6])), p_best[6:11], p_best[11:16], [10,1.7])
visualize(p_best[0], p_best[1:6], p_best[6:11], p_best[11:16], 2000)
