# Line Array Optimiser


## Description
Sound field Prediction for the Meyer Sound MINA loudspeakers, including Nelder-Mead Optimiser to find optimal Trim-hieght, angles, gains and delays.
Implemented for the Optimisation Methods For Engineers Class


## Project status
Project is on hold and is not planned to be developed further in the close future
